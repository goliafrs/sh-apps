#!/bin/bash

set -e

git checkout -f .
git pull
git remote prune origin
git fetch origin --prune
git pull
git fetch --all
git branch -r | awk '{print $1}' | egrep -v -f /dev/fd/0 <(git branch -vv | grep origin) | awk '{print $1}' | xargs git branch -d